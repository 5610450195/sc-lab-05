package control;

import model.Books;
import model.Library;
import model.ReferenceBooks;
import model.Theses;
import model.User;


public class main {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			new main();
		}
		
		public main() {
			testCase();
		}
		
		private void testCase(){
			
			Library library = new Library();
			User user1 = new User("Naruphorn","5610450195","B.A.Student");
			User user2 = new User("Sirada","5610450357","Instructor");
			Books book1 = new Books("Life Hack","Kim	","B75.4.2");
			Books book2 = new Books("Starter for Ten ","David Nichols","H93.8.3");
			Books book3 = new Books("The Awakening ","Kate Chopin","J15.2.1");
			
			Theses thesis1 = new Theses("On-line courseware", "Udutu", "A74.7");
			Theses thesis2 = new Theses("Milky way galaxy ", "Wichean", "D19.5");
			Theses thesis3 = new Theses("Production of coat", "Jukkrit", "Q65.1");
			
			ReferenceBooks refBook = new ReferenceBooks("O'Connor", "Routledge & Kegan Paul");

			library.addBook(book1);
			library.addBook(book2);
			library.addBook(book3);
			
			library.addTheses(thesis1);
			library.addTheses(thesis2);
			library.addTheses(thesis3);
			
			library.addRefBook(refBook);
			
			System.out.println("\n"+"Username: "+user1.getUserName());
			System.out.println("ID: "+user1.getUserId());
			System.out.println("User type: "+user1.getUserType());
			System.out.println("Available zone: "+user1.avaZone());
			System.out.println(">>>Borrower Information<<<"+"\n"+"--- Borrowed 'Life Hack' ---");
			library.borrowedBook(user1, "Life Hack");
			System.out.println("Number of available Books: "+library.getBookCount());
			System.out.println("List of available books: "+library.showBooks());
			
			System.out.println("\n"+"--- Return 'Life Hack' ---");
			library.returnBook(user1,  "Life Hack");
			System.out.println("Number of available Books: "+library.getBookCount());
			System.out.println("List of available books: "+library.showBooks());
			
			System.out.println("\n"+"Username: "+user2.getUserName());
			System.out.println("ID: "+user2.getUserId());
			System.out.println("User type: "+user2.getUserType());
			System.out.println("Available zone: "+user2.avaZone());
			System.out.println(">>>Borrower Information<<<"+"\n"+"--- Borrowed 'On-line courseware' ---");
			library.borrowedTheses(user2, "On-line courseware");
			System.out.println("Number of available Theses: "+library.getThesesCount());
			System.out.println("List of available theses: "+library.showTheses());
			
			System.out.println("\n"+"--- Return 'On-line courseware' ---");
			library.returnTheses(user2, "On-line courseware");
			System.out.println("Number of available Theses: "+library.getThesesCount());
			System.out.println("List of available theses: "+library.showTheses());
			
			System.out.println("\n"+"--- Reference Books **can't borrow ---");
			System.out.println("Number of reference books: "+library.getRefBookCount());
			System.out.println("Reference book: "+library.showReference());
			
		}



}
