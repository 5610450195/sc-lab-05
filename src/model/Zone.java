package model;

public class Zone {
	
	public String zone;
	
	public String someZone1() {
		String zone = "Can go to every zone except the zone for graduate student, instructor and staff.";
		return zone;
	}
	
	public String someZone2() {
		String zone = "Can go to every zone except the zone for staff.";
		return zone;
	}
	
	public String allZone() {
		String place = "Can go to every zone in Library.";
		return zone;
	}
}
