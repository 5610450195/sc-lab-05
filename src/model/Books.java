package model;

public class Books {
	public String title;
	public String author;
	public String callNumber;
	

	public Books(String title, String author, String callNumber) {
		this.title = title;
		this.author = author;
		this.callNumber = callNumber;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public String getCallNumber() {
		return callNumber;
	}
	
}
