package model;

import java.awt.List;
import java.util.ArrayList;

public class Library {

	ArrayList<Books> books = new ArrayList<Books>();
	ArrayList<Books> borrowedBook = new ArrayList<Books>();
	ArrayList<Books> returnBooks = new ArrayList<Books>();
	
	ArrayList<Theses> theses = new ArrayList<Theses>();
	ArrayList<Theses> borrowedTheses = new ArrayList<Theses>();
	ArrayList<Theses> returnTheses = new ArrayList<Theses>();
	
	ArrayList<ReferenceBooks> refBooks = new ArrayList<ReferenceBooks>();

	public void borrowedBook(User user, String title) {
		int count = -1;
		for (int i=0; i<books.size(); i++){
			if(books.get(i).getTitle().equals(title)) {
				count = i;
			}
		}
			
		if (count!=-1) {
			user.addBorrowBook(books.get(count));
			borrowedBook.add(books.get(count));
			returnBooks.add(books.get(count));
			books.remove(count);
		}
	}
	
	public void returnBook(User user,String title) {
		int count = -1;
		for (int i=0; i<returnBooks.size(); i++){
			if(returnBooks.get(i).getTitle().equals(title)) {
				count = i;
			}
		}
			
		if (count!=-1) {
			user.addBorrowBook(books.get(count));
			books.add(returnBooks.get(count));
		}
	}
	
	public void borrowedTheses(User user,String title) {
		int count = -1;
		for (int i=0; i<theses.size(); i++){
			if(theses.get(i).getTitle().equals(title)) {
				count = i;
			}
		}
			
		if (count!=-1) {
			user.addBorrowTheses(theses.get(count));
			borrowedTheses.add(theses.get(count));
			returnTheses.add(theses.get(count));
			theses.remove(count);
		}
		
	}
	
	public void returnTheses(User user,String title) {
		int count = -1;
		for (int i=0; i<returnTheses.size(); i++){
			if(returnTheses.get(i).getTitle().equals(title)) {
				count = i;
			}
		}
			
		if (count!=-1) {
			user.addReturnTheses(theses.get(count));
			theses.add(returnTheses.get(count));
		}
	}
	
	public void addBook(Books addB) {
		books.add(addB);
	}
	
	public void addTheses(Theses addT) {
		theses.add(addT);
	}
	
	public void addRefBook(ReferenceBooks addR) {
		refBooks.add(addR);
	}
	
	public int getBookCount() {
		return books.size();
	}
	
	public int getThesesCount() {
		return theses.size();
	}
	
	public int getRefBookCount() {
		return refBooks.size();
	}
	
	public String showBooks() {
		String everyBooks = "";
		for(Books bookInfo: books) {
			everyBooks += ("\n"+"Title: "+bookInfo.getTitle()+"	Author: "+bookInfo.getAuthor()+
					"	>>call number: "+bookInfo.getCallNumber());
		}
		return everyBooks.substring(0,everyBooks.length());
	}
	
	public String showTheses() {
		String everyTheses = "";
		for(Theses thesesInfo: theses){
			everyTheses += ("\n"+"Title: "+thesesInfo.getTitle()+"	Author: "+thesesInfo.getAuthor()+
					"	>>call number: "+thesesInfo.getCallNumber());
		}
		return everyTheses.substring(0,everyTheses.length());
	}
	
	public String showReference() {
		String everyRef = "";
		for(ReferenceBooks refInfo: refBooks){
			everyRef += ("\n"+"Title: "+refInfo.getTitle()+"	Author: "+refInfo.getAuthor());
		}
		return everyRef.substring(0,everyRef.length());
	}
	
}
