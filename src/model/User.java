package model;

import java.util.ArrayList;

public class User {
	private String name;
	private String id;
	private String type;
	private ArrayList<Books> books = new ArrayList<Books>();
	private ArrayList<Theses> theses = new ArrayList<Theses>();
	Zone zone = new Zone();
	
	public User(String name, String id, String type) {
		this.name = name;
		this.id = id;
		this.type = type;
	}
	
	public String getUserName() {
		return this.name;
	}
	
	public String getUserId() {
		return this.id;
	}
	
	public String getUserType() {
		return this.type;
	}
	
	public void addBorrowBook(Books book) {
		books.add(book);
	}
	
	public void addReturnBook(Books book) {
		books.add(book);
	}
	
	public void addBorrowTheses(Theses thesis) {
		theses.add(thesis);
	}
	
	public void addReturnTheses(Theses thesis) {
		theses.add(thesis);
	}
	
	public String avaZone() {
		String place = null;
		if(this.type.equals("B.A.Student")) {
			place = zone.someZone1();
		}
		if(this.type.equals("M.A.Student")) {
			place = zone.someZone2();
		}
		if(this.type.equals("Ph.D.Student")) {
			place = zone.someZone2();
		}
		if(this.type.equals("Instructor")) {
			place = zone.someZone2();
		}
		if(this.type.equals("Staff")) {
			place = zone.allZone();
		}
		return place;
	}

}
