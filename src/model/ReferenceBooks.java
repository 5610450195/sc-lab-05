package model;

public class ReferenceBooks {
	private String title;
	private String author;
	
	public ReferenceBooks(String title,String author) {
		this.title = title;
		this.author = author;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getAuthor() {
		return author;
	}

}
